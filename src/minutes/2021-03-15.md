# Week 11; Monday March 15th 2021  
3rd week of the month;
1400h EST, 1800h GMT, 2000h CET  
  
Next meeting [Monday March 22nd 2021](/minutes/2021-03-22),  
Prevous meeting [Monday March 8th 2021](/minutes/2021-03-08)  

Original record [office://E5ZzNuXtPmC9gEr25MxgTn](https://office.communitycoins.net/grain/E5ZzNuXtPmC9gEr25MxgTn/)  

## In attendance  
- Sizzle
- Gerrit
- Jeff Bouchard
- Mikael AUR
- Shenki

## Proposed subjects  
- Technology: Atomic Swaps explained: [blog.komodoplatform.com/en/atomic-swaps](https://blog.komodoplatform.com/en/atomic-swaps/)
- Community building: Marketing marketing marketing (Sizzle)
