

# Week 47; Monday November 22nd 2021  
4th week of the month; Core/Development meeting;  
1400h EST, 1800h GMT, 2000h CET  
  
Next meeting [Monday November 29th 2021](/minutes/2021-11-29),  
Prevous meeting [Monday November 15th 2021](/minutes/2021-11-15)

[office://5LRsAxELnRGiim2umDiGRy](https://office.communitycoins.net/grain/5LRsAxELnRGiim2umDiGRy)  


## This meeting has not yet happened,

## In attendance  
- Nothing yet  

## Proposed subjects  
- Nothing yet  

## Off-topic topics discussed  
- Nothing yet  

## Round table  
- Nothing yet  


