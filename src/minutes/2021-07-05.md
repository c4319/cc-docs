

# Week 27; Monday July 5th 2021  
1st week of the month; Development meeting;  
1400h EST, 1800h GMT, 2000h CET  
  
Next meeting [Monday July 12th 2021](/minutes/2021-07-12),  
Prevous meeting [Monday June 28th 2021](/minutes/2021-06-28)

## This meeting has not yet been archived,

## In attendance  
- Nothing yet  

## Proposed subjects  
- Nothing yet  

## Off-topic topics discussed  
- Nothing yet  

## Round table  
- Nothing yet  


