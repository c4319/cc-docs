

# Week 7; Monday February 14th 2022  
2nd week of the month; Core/Executive meeting;  
1400h EST, 1800h GMT, 2000h CET  
  
Next meeting [Monday February 21st 2022](/minutes/2022-02-21),  
Prevous meeting [Monday February 7th 2022](/minutes/2022-02-07)

## This meeting has not yet happened,

## In attendance  
- Nothing yet  

## Proposed subjects  
- Nothing yet  

## Off-topic topics discussed  
- Nothing yet  

## Round table  
- Nothing yet  


