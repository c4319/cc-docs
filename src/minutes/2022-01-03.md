

# Week 1; Monday January 3rd 2022  
1st week of the month; Development meeting;  
1400h EST, 1800h GMT, 2000h CET  
  
Next meeting [Monday January 10th 2022](/minutes/2022-01-10),  
Prevous meeting [Monday December 27th 2021](/minutes/2021-12-27)

## This meeting has not yet happened,

## In attendance  
- Nothing yet  

## Proposed subjects  
- Nothing yet  

## Off-topic topics discussed  
- Nothing yet  

## Round table  
- Nothing yet  


