

# Week 21; Monday May 24th 2021  
4th week of the month; Core/Development meeting;  
1400h EST, 1800h GMT, 2000h CET  
  
Next meeting [Monday May 31st 2021](/minutes/2021-05-31),  
Prevous meeting [Monday May 17th 2021](/minutes/2021-05-17)

## This meeting has not yet been archived,

## In attendance  
- Nothing yet  

## Proposed subjects  
- Nothing yet  

## Off-topic topics discussed  
- Nothing yet  

## Round table  
- Nothing yet  


