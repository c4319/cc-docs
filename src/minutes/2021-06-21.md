

# Week 25; Monday June 21st 2021  
3rd week of the month; Development meeting;  
1400h EST, 1800h GMT, 2000h CET  
  
Next meeting [Monday June 28th 2021](/minutes/2021-06-28),  
Prevous meeting [Monday June 14th 2021](/minutes/2021-06-14)

## This meeting has not yet been archived,

## In attendance  
- Nothing yet  

## Proposed subjects  
- Nothing yet  

## Off-topic topics discussed  
- Nothing yet  

## Round table  
- Nothing yet  


