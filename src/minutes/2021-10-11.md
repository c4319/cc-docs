

# Week 41; Monday October 11th 2021  
2nd week of the month; Core/Executive meeting;  
1400h EST, 1800h GMT, 2000h CET  
  
Next meeting [Monday October 18th 2021](/minutes/2021-10-18),  
Prevous meeting [Monday October 4th 2021](/minutes/2021-10-04)

Original record [office://HZ36hEYh5NfiYe7RXpafGt](https://office.communitycoins.net/grain/HZ36hEYh5NfiYe7RXpafGt/)  

## In attendance  
- Shenki3
- koad
- Mikael
- Aleksej
- Jeff
- gerrit  

## Proposed subjects  
- discussion of first dev day
- various procedural issues ( documented in "Internal Processes")

## Off-topic topics discussed  
- faq
- mailinglist w/analytics
