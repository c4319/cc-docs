# Week 29; Monday July 19th 2021  
3rd week of the month; Development meeting;  
1400h EST, 1800h GMT, 2000h CET  
  
Next meeting [Monday July 26th 2021](/minutes/2021-07-26),  
Prevous meeting [Monday July 12th 2021](/minutes/2021-07-12)

Original record [office://mquoCg9STgcWR7eDmPECqn](https://office.communitycoins.net/grain/mquoCg9STgcWR7eDmPECqn/)  

## In attendance  
- koad
- gerrit
- m.hannes
- dennis

## Proposed subjects
- Unnamed/Unmanned exchange not performing withdrawals
- EFL onto Freiexchange
- EFL testing in cdn-moonshine -> https://github.com/Canada-eCoin/cdn-moonshine
- Paper wallet/voucher write ups
- Virial support material -> https://office.communitycoins.net/grain/DaJPzpNgcAW85M4RFeRBnH
- Curate a list of items that might be helpful to support the idea of our projects going viral.  (killerapp, marketing coordination, black swan)
- Identify things that are preventing us from going viral. (shilling)
- CC Discord -> "you do not have permission to view the message history of #introduce-yourself"

## Off-topic topics discussed:
- Woes of stressful situations / flow / frustration
- Focus to be re-targeted
- Directories of price feed
- CC front end expansion
- Social climate, order/dis-order
- Minutes to be boiled down and organized into several topics and documents (todo://koad)
