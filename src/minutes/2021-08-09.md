

# Week 32; Monday August 9th 2021  
2nd week of the month; Core/Executive meeting;  
1400h EST, 1800h GMT, 2000h CET  
  
Next meeting [Monday August 16th 2021](/minutes/2021-08-16),  
Prevous meeting [Monday August 2nd 2021](/minutes/2021-08-02)

## This meeting has not yet been archived,

## In attendance  
- Nothing yet  

## Proposed subjects  
- Nothing yet  

## Off-topic topics discussed  
- Nothing yet  

## Round table  
- Nothing yet  


