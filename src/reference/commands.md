## koad:io

* `koad configure` - Start the configuration server and spit out a one-time-use link.
* `koad create [entity-name]` - Creates a new koad:io entity in a dot-folder within your home directory
* `koad repair [entity-name]` - Runs the repair tool on an entity that has become critically damaged.
* `koad audit [entity-name]` - Test an entity for curruption and generate a audit report.
* `koad -h` - Print help message and exit.

## entity
Your entity is named by you.  There are secuirty reasons why you name your own program here, but the biggest benefit is that your human mind will remember more details about your entity by context.  This will become clear when you install more than one into your life.

The following commands are included by default in a standard koad:io entity  

* `[entity] configure` - Start the configuration server and spit out a one-time-use link.
* `[entity] create [entity-name]` - Creates a new koad:io entity in a dot-folder within your home directory
* `[entity] repair [entity-name]` - Runs the repair tool on an entity that has become critically damaged.
* `[entity] audit [entity-name]` - Test an entity for curruption and generate a audit report.
* `[entity] -h` - Print help message and exit.
