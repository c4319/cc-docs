# Meeting Minutes

Our minutes are here so our members can add ideas and thoughts that should be covered during the next meetings as well as previous minutes that where recorded;  Some meetings are better recorded than others, mileage may vary.  

---
## Future Meetings
The next few meetings are listed below;  they may or may not contain agendas and potential topics of discussion.

- [Monday Januari 17th 2021](minutes/2022-01-17.md)  
- [Monday Januari 24th 2021](minutes/2022-01-24.md)  
- [Monday Januari 31th 2021](minutes/2022-01-31.md)  
<!-- - [Monday December 20th 2021](/minutes/2021-12-20)   -->
<!-- - [Monday December 27th 2021](/minutes/2021-12-27)  -->

---
## Previous Meeting Minutes  
Each meeting has had a varying amount of record keeping and summarizing.  As we evolve in our group, so does our record keeping.  Below is a list of our previous meetings, not all of them were recorded or transcribed.

- [Monday December 13th 2021](minutes/2021-12-13.md)
- [Monday December 6th 2021](minutes/2021-12-06.md)  
- [Monday November 29nd 2021](minutes/2021-11-29.md)  
- [Monday November 22nd 2021](minutes/2021-11-22.md)  
- [Monday November 15th 2021](minutes/2021-11-15.md)  
- [Monday November 8th 2021](minutes/2021-11-08.md)  
- [Monday November 1st 2021](minutes/2021-11-01.md)  
- Monday October 25th 2021
- Monday October 18th 2021
- [Monday October 11th 2021](minutes/2021-10-11.md)  
- [Monday October 4th 2021](minutes/2021-10-04.md)  
- Monday September 27th 2021
- [Monday September 20th 2021](minutes/2021-09-20.md)  
- Monday September 13th 2021
- Monday September 6th 2021
- Monday August 30th 2021
- Monday August 23rd 2021
- Monday August 16th 2021
- Monday August 9th 2021
- Monday August 2nd 2021
- [Monday July 26th 2021](minutes/2021-07-26.md)  
- [Monday July 19th 2021](minutes/2021-07-19.md)  
- Monday July 12th 2021
- Monday July 5th 2021
- Monday June 28th 2021
- Monday June 21st 2021
- Monday June 14th 2021
- Monday June 7th 2021
- Monday May 31st 2021
- Monday May 24th 2021
- [Monday May 17th 2021](minutes/2021-05-17.md)  
- Monday May 10th 2021
- [Monday May 3rd 2021](minutes/2021-05-03.md)  
- Monday April 26th 2021
- Monday April 19th 2021
- Monday April 12th 2021
- Monday April 5th 2021
- Monday March 29th 2021
- [Monday March 22nd 2021](minutes/2021-03-22.md)  
- [Monday March 15th 2021](minutes/2021-03-15.md)  
