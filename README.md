# cc-docs

Source of the book of communitycoins.

This book contains the proceedings, minutes, posts, publications, readme's produced during the elaboration of Rooty, a collaboration between different communitycoin initiatives.

It builds into a static website that can be accessed at https://cc-docs.communitycoins.org/ or at Gitlab https://c4319.gitlab.io/cc-docs
